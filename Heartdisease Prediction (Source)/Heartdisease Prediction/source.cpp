#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "GL/glut.h"
#include <conio.h>

float draw_scale_x = 0.7;
float draw_scale_y = 0.05;
float draw_offset_x = -5.0;
float draw_offset_y = -2.5;
float scale_x = 0.005;
float scale_y = 0.01;
float offset_x = -1.0;
float offset_y = -1.0;
float _x, _y;

class Iris
{
	std::vector<float> components; //0 = slength, 1 = swidth, 2 = plength, 3 = pwidth
	std::string type;
	bool plucked;
public:
	Iris(float age = 0, float sex = 0, float cpt = 0, float bp = 0, float cl = 0, float bs = 0, float ecg = 0, float mht = 0, float eia = 0, float eistd = 0, float stsps = 0, float fs = 0, float thal = 0, std::string _type = "Iris-plateau")
	{
		components.push_back(age);
		components.push_back(sex);
		components.push_back(cpt);
		components.push_back(bp);
		components.push_back(cl);
		components.push_back(bs);
		components.push_back(ecg);
		components.push_back(mht);
		components.push_back(eia);
		components.push_back(eistd);
		components.push_back(stsps);
		components.push_back(fs);
		components.push_back(thal);
		type = _type;
		plucked = false;
	}

	/*Iris(float age = 0, float sex = 0, float cpt = 0, float bp = 0, float cl = 0, float bs = 0, float ecg = 0, float mht = 0, float eia = 0, float eistd = 0, float stsps = 0, float fs = 0, float thal = 0, float sat = 0, float du = 0, float tig = 0, float emp = 0, float lim = 0, std::string _type = "Iris-plateau")
	{
		components.push_back(age);
		components.push_back(sex);
		components.push_back(cpt);
		components.push_back(bp);
		components.push_back(cl);
		components.push_back(bs);
		components.push_back(ecg);
		components.push_back(mht);
		components.push_back(eia);
		components.push_back(eistd);
		components.push_back(stsps);
		components.push_back(fs);
		components.push_back(thal);
		components.push_back(sat);
		components.push_back(du);
		components.push_back(tig);
		components.push_back(emp);
		components.push_back(lim);
		type = _type;
		plucked = false;
	}*/

	float GetComponent(int index)
	{
		return components[index];
	}

	int GetComponentSize()
	{
		return components.size();
	}

	void SetComponent(float value, int index)
	{
		components[index] = value;
	}

	void SetType(std::string _type)
	{
		type = _type;
	}

	std::string GetType()
	{
		return type;
	}

	void Pluck(bool status)
	{
		plucked = status;
	}

	bool IsPlucked()
	{
		return plucked;
	}
};

class Bouquet
{
	std::vector<Iris> flowers;
public:
	void AddFlower(Iris &flower)
	{
		flowers.push_back(flower);
	}

	void LoadFromFile(std::string filename)
	{
		Iris dummy;
		std::string line;
		std::ifstream rfile(filename);
		while (rfile >> line)
		{
			for (int i = 0; i < dummy.GetComponentSize(); i++)
			{
				dummy.SetComponent(std::stof(line.substr(0, line.find(','))), i);
				line.erase(line.begin() + 0, line.begin() + line.find(',') + 1);
			}
			dummy.SetType(line);
			flowers.push_back(dummy);
		}
		rfile.close();
	}

	void PrintAll()
	{
		for (int i = 0; i < flowers.size(); i++)
		{
			for (int j = 0; j < flowers[i].GetComponentSize(); j++)
			{
				std::cout << flowers[i].GetComponent(j) << "\t";
			}
			std::cout << flowers[i].GetType() << "\n";
		}
	}

	int GetFlowerCount()
	{
		return flowers.size();
	}

	Iris* GetFlower(int index)
	{
		return &flowers[index];
	}
};

float CalculateDistance(Iris *one, Iris *two)
{
	float distance = 0;
	for (int i = 0; i < one->GetComponentSize(); i++)
	{
		distance += (float)pow((float)(one->GetComponent(i) - two->GetComponent(i)), (float)2.0);
	}
	return pow((float)distance, (float)0.5);
}

float CalculateDistanceNormalized(Iris *one, Iris *two, std::vector<float> &min, std::vector<float> &max)
{
	float distance = 0;
	float val1, val2;
	for (int i = 0; i < one->GetComponentSize(); i++)
	{
		val1 = (one->GetComponent(i) - min[i]) / (max[i] - min[i]);
		val2 = (two->GetComponent(i) - min[i]) / (max[i] - min[i]);
		distance += (float)pow((float)(val1 - val2), (float)2.0);
	}
	return pow((float)distance, (float)0.5);
}

void CountString(std::vector<int> &stringCount, std::vector<std::string> &stringArr, std::string find)
{
	for (int i = 0; i < stringArr.size(); i++)
	{
		if (stringArr[i] == find)
		{
			stringCount[i]++;
			return;
		}
	}
	stringArr.push_back(find);
	stringCount.push_back(1);
}

void SortStringDesc(std::vector<int> &stringCount, std::vector<std::string> &stringArr)
{
	int temp;
	std::string tempstr;
	for (int i = 0; i < stringCount.size(); i++)
	{
		for (int j = i; j < stringCount.size(); j++)
		{
			if (stringCount[j] > stringCount[i])
			{
				temp = stringCount[i];
				stringCount[i] = stringCount[j];
				stringCount[j] = temp;

				tempstr = stringArr[i];
				stringArr[i] = stringArr[j];
				stringArr[j] = tempstr;
			}
		}
	}
}

void FilterString(std::vector<std::string> &stringArr, std::string find)
{
	for (int i = 0; i < stringArr.size(); i++)
	{
		if (find == stringArr[i])
		{
			return;
		}
	}
	stringArr.push_back(find);
}

float GaussianFunction(float mean, float variance, float x)
{
	float e = 2.71828182846;
	float divisor = (float)pow((float)((2 * 22 * variance) / 7), (float)0.5);
	float power = (-1) * ((float)pow((float)(x - mean), (float)2.0) / (2 * variance));
	float gauss = (float)pow(e, power) / divisor;
	return gauss;
}

void DoubleSort(std::vector<int> &index, std::vector<float> &distance)
{
	int tempi;
	float temp;
	for (int i = 0; i < distance.size(); i++)
	{
		for (int j = i; j < distance.size(); j++)
		{
			if (distance[j] < distance[i])
			{
				temp = distance[i];
				distance[i] = distance[j];
				distance[j] = temp;

				tempi = index[i];
				index[i] = index[j];
				index[j] = tempi;
			}
		}
	}
}

void SortAscending(std::vector<float> &distance)
{
	float temp;
	for (int i = 0; i < distance.size(); i++)
	{
		for (int j = i; j < distance.size(); j++)
		{
			if (distance[j] < distance[i])
			{
				temp = distance[i];
				distance[i] = distance[j];
				distance[j] = temp;
			}
		}
	}
}

class Rochette
{
	Bouquet bouquet, bush;
	std::vector<std::vector<Iris*>> vase;
	std::vector<float> errors;
	std::vector<float> knnAverage;
	std::vector<std::string> stringArr;
	std::vector<int> stringCount;
	std::vector<std::vector<float>> mean;
	std::vector<std::vector<float>> variance;
	std::vector<float> f1value;
	std::vector<int> f1index;
	float distance;
	int req, k;

public:
	Rochette()
	{
		distance = 3.0;
		req = 3;
	}

	void SetBouquet(Bouquet &data)
	{
		bouquet = data;
		req = data.GetFlower(0)->GetComponentSize() + 1;
		k = req - 1;

		std::cout << "Dimension Size : " << (req - 1) << "\n";
	}

	void SetBush(Bouquet &data)
	{
		bush = data;
	}

	void IdentifyBush()
	{
		//std::cout << "Identifying Bush..\n";
		std::ofstream wfile("predict.txt");

		std::vector<std::string> stringArr;
		std::vector<std::string> results;
		std::vector<int> stringCount;
		std::vector<int> neighbors;
		std::vector<float> distances;
		std::vector<Iris> temp;
		std::vector<float> min, max;

		for (int j = 0; j < bouquet.GetFlower(0)->GetComponentSize(); j++)
		{
			for (int i = 0; i < bouquet.GetFlowerCount(); i++)
			{
				distances.push_back(bouquet.GetFlower(i)->GetComponent(j));
			}
			SortAscending(distances);
			min.push_back(distances[0]);
			max.push_back(distances[distances.size() - 1]);
			distances.clear();
		}

		float cluster;
		for (int i = 0; i < bush.GetFlowerCount(); i++)
		{
			//std::cout << "Iteration : " << i << "\n";
			for (int j = 0; j < bouquet.GetFlowerCount(); j++)
			{
				neighbors.push_back(j);
				distances.push_back(CalculateDistanceNormalized(bush.GetFlower(i), bouquet.GetFlower(j), min, max));
			}
			
			DoubleSort(neighbors, distances);

			for (int j = 0; j < k; j++)
			{
				temp.push_back(*bouquet.GetFlower(neighbors[j]));
			}

			for (int j = 0; j < k; j++)
			{
				CountString(stringCount, stringArr, temp[j].GetType());
			}

			SortStringDesc(stringCount, stringArr);

			//for (int j = 0; j < stringCount.size(); j++)
			//{
			//	std::cout << stringArr[j] << " : " << stringCount[j] << "\n";
			//}

			wfile << stringArr[0] << "\n";

			results.push_back(stringArr[0]);

			stringArr.clear();
			stringCount.clear();
			neighbors.clear();
			distances.clear();
			temp.clear();
		}

		wfile.close();

		//std::cout << "Success!\n";

		IdentificationAccuracy(results);
	}

	void IdentificationAccuracy(std::vector<std::string> &results)
	{
		float accuracy = 0, precision, recall, f1;
		float p = 0, n = 0;
		float tp = 0, fp = 0, fn = 0;
		//std::cout << "Expected\tResults\n";
		for (int i = 0; i < bush.GetFlowerCount(); i++)
		{
			//std::cout << results[i] << "\t\t" << bush.GetFlower(i)->GetType() << "\n";
			if (results[i] == bush.GetFlower(i)->GetType())
			{
				accuracy++;
			}
			if (bush.GetFlower(i)->GetType() == "1")
			{
				p++;
				if (results[i] == "1")
				{
					tp++;
				}
				else
				{
					fn++;
				}
			}
			else
			{
				n++;
				if (results[i] == "1")
				{
					fp++;
				}
			}
		}
		accuracy = (accuracy * 100) / bush.GetFlowerCount();
		precision = (tp * 100) / (tp + fp);
		recall = (tp * 100) / (tp + fn);
		f1 = (2 * (precision / 100) * (recall / 100)) / ((precision / 100) + (recall / 100));
		std::cout << "Accuracy  : " << accuracy << "%\t";
		std::cout << "Precision : " << precision << "%\t";
		if (k <= 2)
		{
			std::cout << "\t";
		}
		std::cout << "Recall    : " << recall << "%\t";
		std::cout << "F1        : " << f1 << "\n";

		f1value.push_back(f1);
		f1index.push_back(k);
	}

	void BestResult()
	{
		DoubleSort(f1index, f1value);
		
		k = f1index[f1index.size() - 1];

		std::cout << "K = " << k << "\n";
		//std::cout << "Identifying Bush..\n";
		std::ofstream wfile("predict.txt");

		std::vector<std::string> stringArr;
		std::vector<std::string> results;
		std::vector<int> stringCount;
		std::vector<int> neighbors;
		std::vector<float> distances;
		std::vector<Iris> temp;
		std::vector<float> min, max;

		for (int j = 0; j < bouquet.GetFlower(0)->GetComponentSize(); j++)
		{
			for (int i = 0; i < bouquet.GetFlowerCount(); i++)
			{
				distances.push_back(bouquet.GetFlower(i)->GetComponent(j));
			}
			SortAscending(distances);
			min.push_back(distances[0]);
			max.push_back(distances[distances.size() - 1]);
			distances.clear();
		}

		float cluster;
		for (int i = 0; i < bush.GetFlowerCount(); i++)
		{
			//std::cout << "Iteration : " << i << "\n";
			for (int j = 0; j < bouquet.GetFlowerCount(); j++)
			{
				neighbors.push_back(j);
				distances.push_back(CalculateDistanceNormalized(bush.GetFlower(i), bouquet.GetFlower(j), min, max));
			}
			
			DoubleSort(neighbors, distances);

			for (int j = 0; j < k; j++)
			{
				temp.push_back(*bouquet.GetFlower(neighbors[j]));
			}

			for (int j = 0; j < k; j++)
			{
				CountString(stringCount, stringArr, temp[j].GetType());
			}

			SortStringDesc(stringCount, stringArr);

			//for (int j = 0; j < stringCount.size(); j++)
			//{
			//	std::cout << stringArr[j] << " : " << stringCount[j] << "\n";
			//}

			wfile << stringArr[0] << "\n";

			results.push_back(stringArr[0]);

			stringArr.clear();
			stringCount.clear();
			neighbors.clear();
			distances.clear();
			temp.clear();
		}

		wfile.close();

		//std::cout << "Success!\n";

		float accuracy = 0, precision, recall, f1;
		float p = 0, n = 0;
		float tp = 0, fp = 0, fn = 0;
		std::cout << "Expected\tResults\n";
		for (int i = 0; i < bush.GetFlowerCount(); i++)
		{
			std::cout << results[i] << "\t\t" << bush.GetFlower(i)->GetType() << "\n";
			if (results[i] == bush.GetFlower(i)->GetType())
			{
				accuracy++;
			}
			if (bush.GetFlower(i)->GetType() == "1")
			{
				p++;
				if (results[i] == "1")
				{
					tp++;
				}
				else
				{
					fn++;
				}
			}
			else
			{
				n++;
				if (results[i] == "1")
				{
					fp++;
				}
			}
		}

		accuracy = (accuracy * 100) / bush.GetFlowerCount();
		precision = (tp * 100) / (tp + fp);
		recall = (tp * 100) / (tp + fn);
		f1 = (2 * (precision / 100) * (recall / 100)) / ((precision / 100) + (recall / 100));
		std::cout << "Accuracy  : " << accuracy << "%\t";
		std::cout << "Precision : " << precision << "%\t";
		if (k <= 2)
		{
			std::cout << "\t";
		}
		std::cout << "Recall    : " << recall << "%\t";
		std::cout << "F1        : " << f1 << "\n";
	}

	void CalculateStatistics()
	{
		for (int i = 0; i < bouquet.GetFlowerCount(); i++)
		{
			CountString(stringCount, stringArr, bouquet.GetFlower(i)->GetType());
			//FilterString(stringArr, bouquet.GetFlower(i)->GetType());
		}

		CalculateMean();
		CalculateVariance();

		for (int i = 0; i < stringArr.size(); i++)
		{
			std::cout << "Class : " << stringArr[i] << "\n";
			for (int j = 0; j < bouquet.GetFlower(0)->GetComponentSize(); j++)
			{
				std::cout << "Mean - " << (j + 1) << " : " << mean[i][j] << "\n";
			}

			for (int j = 0; j < bouquet.GetFlower(0)->GetComponentSize(); j++)
			{
				std::cout << "Variance - " << (j + 1) << " : " << variance[i][j] << "\n";
			}
		}
	}

	void CalculateMean()
	{
		for (int k = 0; k < stringArr.size(); k++)
		{
			//std::cout << stringArr[k] << "\n";
			mean.push_back(std::vector<float>());
			for (int i = 0; i < bouquet.GetFlower(0)->GetComponentSize(); i++)
			{
				mean[k].push_back(0);
				for (int j = 0; j < bouquet.GetFlowerCount(); j++)
				{
					if (bouquet.GetFlower(j)->GetType() == stringArr[k])
					{
						mean[k][mean[k].size() - 1] += bouquet.GetFlower(j)->GetComponent(i);
					}
				}
				mean[k][mean[k].size() - 1] /= stringCount[k];
			}
		}
	}

	void CalculateVariance()
	{
		for (int k = 0; k < stringArr.size(); k++)
		{
			//std::cout << stringCount[k] << "\n";
			variance.push_back(std::vector<float>());
			for (int i = 0; i < bouquet.GetFlower(0)->GetComponentSize(); i++)
			{
				variance[k].push_back(0);
				for (int j = 0; j < bouquet.GetFlowerCount(); j++)
				{
					if (bouquet.GetFlower(j)->GetType() == stringArr[k])
					{
						variance[k][variance[k].size() - 1] += (float)pow((float)(bouquet.GetFlower(j)->GetComponent(i) - mean[k][i]), (float)2.0);
					}
				}
				variance[k][variance[k].size() - 1] /= (stringCount[k] - 1);
			}
		}
	}

	int GetK()
	{
		return k;
	}

	int GetColSize()
	{
		return bouquet.GetFlower(0)->GetComponentSize();
	}

	void SetK(int _k)
	{
		k = _k;
	}
};

Bouquet iris, bush;
std::vector<Rochette> mk1;

void main()
{
	iris.LoadFromFile("heartdisease-train.csv");
	bush.LoadFromFile("heartdisease-test.csv");

	mk1.push_back(Rochette());
	mk1[mk1.size() - 1].SetBouquet(iris);
	mk1[mk1.size() - 1].SetBush(bush);

	std::cout << "Testing K values, Range : 1 - " << mk1[mk1.size() - 1].GetK() << " on test-data.csv\n";
	for (int i = 0; i < mk1[mk1.size() - 1].GetColSize(); i++)
	{
		std::cout << (i + 1) << " - ";
		mk1[mk1.size() - 1].SetK(i + 1);
		mk1[mk1.size() - 1].IdentifyBush();
	}
	
	std::cout << "\nBest Result : \n";

	mk1[mk1.size() - 1].BestResult();

	mk1[mk1.size() - 1].SetBush(iris);

	std::cout << "Testing K = " << mk1[mk1.size() - 1].GetK() << " on train-data.csv\n";
	mk1[mk1.size() - 1].BestResult();

	std::cout << "Press any key to exit the program..\n";
	getch();
	
	//mk1[mk1.size() - 1].CalculateStatistics();
	//mk1[mk1.size() - 1].NaiveBush();
}